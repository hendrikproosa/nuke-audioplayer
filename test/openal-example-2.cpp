/*
 * OpenAL example
 *
 * Copyright(C) Florian Fainelli <f.fainelli@gmail.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <inttypes.h>
#include <unistd.h>
#include <stdbool.h>
#include <chrono>
#include <thread>

#include <AL/al.h>
#include <AL/alc.h>

#include <AL/alut.h>
#define BACKEND "alut"


static void list_audio_devices(const ALCchar *devices)
{
	const ALCchar *device = devices, *next = devices + 1;
	size_t len = 0;

	fprintf(stdout, "Devices list:\n");
	fprintf(stdout, "----------\n");
	while (device && *device != '\0' && next && *next != '\0') {
		fprintf(stdout, "%s\n", device);
		len = strlen(device);
		device += (len + 1);
		next += (len + 2);
	}
	fprintf(stdout, "----------\n");
}


#define TEST_ERROR(_msg)		\
	error = alGetError();		\
	if (error != AL_NO_ERROR) {	\
		fprintf(stderr, _msg "\n");	\
		return -1;		\
	}


// Calculates the length of source buffer in seconds
// based on different audio parameters and buffer size
ALfloat getBufferLengthInSec(ALuint buffer)
{
	ALint size, bits, channels, freq;
	alGetBufferi(buffer, AL_SIZE, &size);
	alGetBufferi(buffer, AL_BITS, &bits);
	alGetBufferi(buffer, AL_CHANNELS, &channels);
	alGetBufferi(buffer, AL_FREQUENCY, &freq);
	if (alGetError() != AL_NO_ERROR)
		return -1.0f;

	return (ALfloat)((ALuint)size/channels/(bits/8)) / (ALfloat)freq;
}


// Repositions source buffer offset based on relative seek position
ALuint playFromSeek(ALuint source, ALuint buffer, ALfloat seek)
{ 
	if (seek < 0 || seek > 1)
		return 1; //stopping if seek is invalid

	alSourcei(source, AL_BUFFER, buffer); //retrieving source's buffer
	ALint tot = 0;
	alGetBufferi(buffer, AL_SIZE, &tot); //size of buffer (in bytes)
	alSourcei(source, AL_BYTE_OFFSET, seek*tot); //positionning...
	alSourcePlay(source); //let's play
	return source;
}


void playFromSec(ALuint source, ALuint buffer, ALfloat sec)
{
	// Play from relative position between source start and end
	// defined by seek position and total length of source
	playFromSeek(source, buffer, sec/getBufferLengthInSec(buffer));
}



int main(int argc, char **argv)
{
	ALboolean enumeration;
	const ALCchar *devices;
	const ALCchar *defaultDeviceName = argv[1];
	int ret;
	char *bufferData;
	ALCdevice *device;
	ALvoid *data, *data2;
	ALCcontext *context;
	ALsizei size, size2, freq, freq2;
	ALenum format, format2;
	ALuint buffer, buffer2, source, source2;
	ALfloat listenerOri[] = { 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f };
	ALboolean loop = AL_FALSE;
	ALCenum error;
	ALint source_state;

	alutInit(NULL, NULL);

	fprintf(stdout, "Using " BACKEND " as audio backend\n");

	enumeration = alcIsExtensionPresent(NULL, "ALC_ENUMERATION_EXT");
	if (enumeration == AL_FALSE)
		fprintf(stderr, "enumeration extension not available\n");

	list_audio_devices(alcGetString(NULL, ALC_DEVICE_SPECIFIER));

	if (!defaultDeviceName)
		defaultDeviceName = alcGetString(NULL, ALC_DEFAULT_DEVICE_SPECIFIER);

	device = alcOpenDevice(defaultDeviceName);
	if (!device) {
		fprintf(stderr, "unable to open default device\n");
		return -1;
	}

	fprintf(stdout, "Device: %s\n", alcGetString(device, ALC_DEVICE_SPECIFIER));

	alGetError();

	context = alcCreateContext(device, NULL);
	if (!alcMakeContextCurrent(context)) {
		fprintf(stderr, "failed to make default context\n");
		return -1;
	}
	TEST_ERROR("make default context");

	/* set orientation */
	alListener3f(AL_POSITION, 0, 0, 1.0f);
	TEST_ERROR("listener position");
    alListener3f(AL_VELOCITY, 0, 0, 0);
	TEST_ERROR("listener velocity");
	alListenerfv(AL_ORIENTATION, listenerOri);
	TEST_ERROR("listener orientation");

	alGenSources((ALuint)1, &source);
	TEST_ERROR("source generation");
	alGenSources((ALuint)1, &source2);
	TEST_ERROR("source generation");

	alSourcef(source, AL_PITCH, 1);
	TEST_ERROR("source pitch");
	alSourcef(source, AL_GAIN, 1);
	TEST_ERROR("source gain");
	alSource3f(source, AL_POSITION, 0, 0, 0);
	TEST_ERROR("source position");
	alSource3f(source, AL_VELOCITY, 0, 0, 0);
	TEST_ERROR("source velocity");
	alSourcei(source, AL_LOOPING, AL_FALSE);
	TEST_ERROR("source looping");

	alSourcef(source2, AL_PITCH, 1);
	TEST_ERROR("source pitch");
	alSourcef(source2, AL_GAIN, 0.3);
	TEST_ERROR("source gain");
	alSource3f(source2, AL_POSITION, 0, 0, 0);
	TEST_ERROR("source position");
	alSource3f(source2, AL_VELOCITY, 0, 0, 0);
	TEST_ERROR("source velocity");
	alSourcei(source2, AL_LOOPING, AL_FALSE);
	TEST_ERROR("source looping");

	alGenBuffers(1, &buffer);
	TEST_ERROR("buffer generation");
	alGenBuffers(1, &buffer2);
	TEST_ERROR("buffer generation");

	// alutLoadWAVFile((ALbyte*)"PinkPanther30.wav", &format, &data, &size, &freq, &loop);
	// TEST_ERROR("loading wav file");
	//alutLoadWAVFile((ALbyte*)"ImperialMarch60.wav", &format2, &data2, &size2, &freq2, &loop);
	// TEST_ERROR("loading wav file");
	buffer = alutCreateBufferFromFile("PinkPanther30.wav");
	buffer2 = alutCreateBufferFromFile("StarWars60.wav");

	// alBufferData(buffer, format, data, size, freq);
	// TEST_ERROR("buffer copy");
	//alBufferData(buffer2, format2, data2, size2, freq2);
	//TEST_ERROR("buffer copy");

	alSourcei(source, AL_BUFFER, buffer);
	TEST_ERROR("buffer binding");
	alSourcei(source2, AL_BUFFER, buffer2);
	TEST_ERROR("buffer binding");

	playFromSec(source, buffer, 0.0f);
	// alSourcePlay(source);
	TEST_ERROR("source playing");
	playFromSec(source2, buffer2, 0.0f);

	alGetSourcei(source, AL_SOURCE_STATE, &source_state);
	TEST_ERROR("source state get");

	int i = 0;
	int playUntil = 150;
	while (source_state == AL_PLAYING && i < playUntil) {
		alGetSourcei(source, AL_SOURCE_STATE, &source_state);
		TEST_ERROR("source state get");
		// float gain = (float)i / (float)playUntil;
		// alSourcef(source, AL_GAIN, gain);
		// TEST_ERROR("source gain");
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		i++;
	}

	/* exit context */
	alDeleteSources(1, &source);
	alDeleteSources(1, &source2);
	alDeleteBuffers(1, &buffer);
	alDeleteBuffers(1, &buffer2);
	device = alcGetContextsDevice(context);
	alcMakeContextCurrent(NULL);
	alcDestroyContext(context);
	alcCloseDevice(device);

	return 0;
}
