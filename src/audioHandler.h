#ifndef AUDIOHANDLER_H
#define AUDIOHANDLER_H

// General includes
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <math.h>
#include <bitset>
#include <assert.h>
#include <cmath>
#include <stdint.h>
#include <thread>
#include <chrono>
#include <errno.h>
#include <inttypes.h>
#include <unistd.h>
#include <stdbool.h>
#include <cstring>
#include <fstream>

// OpenAL
#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alut.h>

const std::size_t NUM_BUFFERS = 3;
const std::size_t BUFFER_SIZE = 65536; // 32kb of data in each buffer

enum class endian
{
#ifdef _WIN32
    little = 0,
    big    = 1,
    native = little
#else
    little = __ORDER_LITTLE_ENDIAN__,
    big    = __ORDER_BIG_ENDIAN__,
    native = __BYTE_ORDER__
#endif
};


inline std::int16_t convert_to_short(char* buffer, std::size_t len, bool swap)
{
    std::int16_t a = 0;
    // if (endian::native == endian::little)
    if (!swap)
        std::memcpy(&a, buffer, len);
    else
        for(std::size_t i = 0; i < len; ++i)
            reinterpret_cast<char*>(&a)[len - 1 - i] = buffer[i];
    return a;
}


class AudioHandler
{
public:
    AudioHandler();
    ~AudioHandler();

    // Loads file and initializes related decoder side stuff
    void initializeHandler();
    void releaseHandler();

    void loadFile(const char* fileName);
    void generateWaveform(int pixelWidth, int pixelHeight);

    void playFile();
    void playFrame(int frame);
    void setGain(float gain);
    void setPitch(float pitch);
    void setFps(float newFps);
    void setChunkSize(int chunkSize);

    float* getWaveformL() { return waveformDataL; }
    float* getWaveformR() { return waveformDataR; }
    int getLastPlayedFrame() { return _lastPlayedFrame; }
    int getFrameSizeAsBytes(float fps);
    int getFileLengthInFrames();
    char* getData() { return data; }
    int getBitsPerSample() { return bitsPerSample; }
    int getEmptyBuffersCount();
    std::vector<int> getFramesInQueue();
    int getWaveformWidth() { return waveformWidth; }

    bool update_stream(const ALuint source, const ALenum& format, const std::int32_t& sampleRate, char* soundData, int soundDataSize, int currentFrame);

    bool fileLoaded() { return _fileLoaded; }
    void setFileLoaded(bool isLoaded) { _fileLoaded = isLoaded; };

private:
    int messageLevel;
    bool _fileLoaded;
    int _chunkSize;

    const ALCchar *devices;
	char *bufferData;
	ALCdevice *device;
	ALCcontext *context;
	ALsizei size, freq;
	ALuint buffer, source;

    ALuint buffers[NUM_BUFFERS];
    char* data;
    ALsizei dataSize;
    ALenum format;
    std::uint8_t channels;
    std::int32_t sampleRate;
    std::uint8_t bitsPerSample;

    float _fps;
    int _lastPlayedFrame;
    std::vector<int> queueFrames;

    float* waveformDataL;
    float* waveformDataR;
    int waveformWidth;
};

#endif // AUDIOHANDLER_H