#include "audioHandler.h"

#include <vector>
#include <iterator>

static void list_audio_devices(const ALCchar *devices)
{
	const ALCchar *device = devices, *next = devices + 1;
	size_t len = 0;

	fprintf(stdout, "Devices list:\n");
	fprintf(stdout, "----------\n");
	while (device && *device != '\0' && next && *next != '\0') {
		fprintf(stdout, "%s\n", device);
		len = strlen(device);
		device += (len + 1);
		next += (len + 2);
	}
	fprintf(stdout, "----------\n");
}


#define TEST_ERROR(_msg)		\
	error = alGetError();		\
	if (error != AL_NO_ERROR) {	\
		fprintf(stderr, _msg "\n");	\
		return -1;		\
	}

int testALError(const char* _msg)
{
    ALCenum error;
    error = alGetError();
    if (error != AL_NO_ERROR)
        std::cout << _msg << std::endl;
        
    return error;
}


// Calculates the length of source buffer in seconds
// based on different audio parameters and buffer size
ALfloat getBufferLengthInSec(ALuint buffer)
{
	ALint size, bits, channels, freq;
	alGetBufferi(buffer, AL_SIZE, &size);
	alGetBufferi(buffer, AL_BITS, &bits);
	alGetBufferi(buffer, AL_CHANNELS, &channels);
	alGetBufferi(buffer, AL_FREQUENCY, &freq);
	if (alGetError() != AL_NO_ERROR)
		return 0.0f;

	return (ALfloat)((ALuint)size/channels/(bits/8)) / (ALfloat)freq;
}


// Repositions source buffer offset based on relative seek position
ALuint playFromSeek(ALuint source, ALuint buffer, ALfloat seek)
{ 
	if (seek < 0 || seek > 1)
		return 1; //stopping if seek is invalid

	alSourcei(source, AL_BUFFER, buffer); //retrieving source's buffer
	ALint tot = 0;
	alGetBufferi(buffer, AL_SIZE, &tot); //size of buffer (in bytes)
	alSourcei(source, AL_BYTE_OFFSET, seek*tot); //positionning...
	alSourcePlay(source); //let's play
	return source;
}


void playFromSec(ALuint source, ALuint buffer, ALfloat sec)
{
	// Play from relative position between source start and end
	// defined by seek position and total length of source
	playFromSeek(source, buffer, sec/getBufferLengthInSec(buffer));
}


std::int32_t convert_to_int(char* buffer, std::size_t len)
{
    std::int32_t a = 0;
    if(endian::native == endian::little)
        std::memcpy(&a, buffer, len);
    else
        for(std::size_t i = 0; i < len; ++i)
            reinterpret_cast<char*>(&a)[3 - i] = buffer[i];
    return a;
}


bool load_wav_file_header(std::ifstream& file,
                          std::uint8_t& channels,
                          std::int32_t& sampleRate,
                          std::uint8_t& bitsPerSample,
                          ALsizei& size)
{
    char buffer[4];
    if(!file.is_open())
        return false;

    // the RIFF
    if(!file.read(buffer, 4))
    {
        std::cerr << "ERROR: could not read RIFF" << std::endl;
        return false;
    }
    if(std::strncmp(buffer, "RIFF", 4) != 0)
    {
        std::cerr << "ERROR: file is not a valid WAVE file (header doesn't begin with RIFF)" << std::endl;
        return false;
    }

    // the size of the file
    if(!file.read(buffer, 4))
    {
        std::cerr << "ERROR: could not read size of file" << std::endl;
        return false;
    }

    // the WAVE
    if(!file.read(buffer, 4))
    {
        std::cerr << "ERROR: could not read WAVE" << std::endl;
        return false;
    }
    if(std::strncmp(buffer, "WAVE", 4) != 0)
    {
        std::cerr << "ERROR: file is not a valid WAVE file (header doesn't contain WAVE)" << std::endl;
        return false;
    }

    // "fmt/0"
    if(!file.read(buffer, 4))
    {
        std::cerr << "ERROR: could not read fmt/0" << std::endl;
        return false;
    }

    // this is always 16, the size of the fmt data chunk
    if(!file.read(buffer, 4))
    {
        std::cerr << "ERROR: could not read the 16" << std::endl;
        return false;
    }

    // PCM should be 1?
    if(!file.read(buffer, 2))
    {
        std::cerr << "ERROR: could not read PCM" << std::endl;
        return false;
    }

    // the number of channels
    if(!file.read(buffer, 2))
    {
        std::cerr << "ERROR: could not read number of channels" << std::endl;
        return false;
    }
    channels = convert_to_int(buffer, 2);

    // sample rate
    if(!file.read(buffer, 4))
    {
        std::cerr << "ERROR: could not read sample rate" << std::endl;
        return false;
    }
    sampleRate = convert_to_int(buffer, 4);

    // (sampleRate * bitsPerSample * channels) / 8
    if(!file.read(buffer, 4))
    {
        std::cerr << "ERROR: could not read (sampleRate * bitsPerSample * channels) / 8" << std::endl;
        return false;
    }

    // ?? dafaq
    if(!file.read(buffer, 2))
    {
        std::cerr << "ERROR: could not read dafaq" << std::endl;
        return false;
    }

    // bitsPerSample
    if(!file.read(buffer, 2))
    {
        std::cerr << "ERROR: could not read bits per sample" << std::endl;
        return false;
    }
    bitsPerSample = convert_to_int(buffer, 2);

    // data chunk header "data"
    if(!file.read(buffer, 4))
    {
        std::cerr << "ERROR: could not read data chunk header" << std::endl;
        return false;
    }
    if(std::strncmp(buffer, "data", 4) != 0)
    {
        std::cerr << "ERROR: file is not a valid WAVE file (doesn't have 'data' tag)" << std::endl;
        return false;
    }

    // size of data
    if(!file.read(buffer, 4))
    {
        std::cerr << "ERROR: could not read data size" << std::endl;
        return false;
    }
    size = convert_to_int(buffer, 4);

    /* cannot be at the end of file */
    if(file.eof())
    {
        std::cerr << "ERROR: reached EOF on the file" << std::endl;
        return false;
    }
    if(file.fail())
    {
        std::cerr << "ERROR: fail state set on the file" << std::endl;
        return false;
    }

    return true;
}

char* load_wav(const std::string& filename,
               std::uint8_t& channels,
               std::int32_t& sampleRate,
               std::uint8_t& bitsPerSample,
               ALsizei& size)
{
    std::ifstream in(filename, std::ios::binary);
    if(!in.is_open())
    {
        std::cerr << "ERROR: Could not open \"" << filename << "\"" << std::endl;
        return nullptr;
    }
    if(!load_wav_file_header(in, channels, sampleRate, bitsPerSample, size))
    {
        std::cerr << "ERROR: Could not load wav header of \"" << filename << "\"" << std::endl;
        return nullptr;
    }

    char* data = new char[size];
    in.read(data, size);

    return data;
}



AudioHandler::AudioHandler()
{
    _fileLoaded = false;
    data = nullptr;
    _fps = 25.0;
    _lastPlayedFrame = 0;
    _chunkSize = 25;
    waveformDataL = nullptr;
    waveformDataR = nullptr;
    waveformWidth = 0;

    initializeHandler();
}


AudioHandler::~AudioHandler()
{
    /* exit context */
	alDeleteSources(1, &source);
	alDeleteBuffers(1, &buffer);
	device = alcGetContextsDevice(context);
	alcMakeContextCurrent(NULL);
	alcDestroyContext(context);
	alcCloseDevice(device);
}


void AudioHandler::initializeHandler()
{
    ALboolean enumeration;
	ALfloat listenerOri[] = { 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f };

	alutInit(NULL, NULL);

	enumeration = alcIsExtensionPresent(NULL, "ALC_ENUMERATION_EXT");
	if (enumeration == AL_FALSE)
		fprintf(stderr, "enumeration extension not available\n");

	list_audio_devices(alcGetString(NULL, ALC_DEVICE_SPECIFIER));

	const ALCchar *defaultDeviceName = alcGetString(NULL, ALC_DEFAULT_DEVICE_SPECIFIER);

	device = alcOpenDevice(defaultDeviceName);
	if (!device) {
		fprintf(stderr, "unable to open default device\n");
		return;
	}

	fprintf(stdout, "Device: %s\n", alcGetString(device, ALC_DEVICE_SPECIFIER));

	alGetError();

	context = alcCreateContext(device, NULL);
	if (!alcMakeContextCurrent(context)) {
		fprintf(stderr, "failed to make default context\n");
		return;
	}
	testALError("make default context");

	/* set orientation */
	alListener3f(AL_POSITION, 0, 0, 1.0f);
	testALError("listener position");
    alListener3f(AL_VELOCITY, 0, 0, 0);
	testALError("listener velocity");
	alListenerfv(AL_ORIENTATION, listenerOri);
	testALError("listener orientation");
}

void AudioHandler::releaseHandler()
{
    alDeleteSources(1, &source);
	alDeleteBuffers(1, &buffer);
    delete[] data;
}


void AudioHandler::loadFile(const char* fileName)
{
    if (fileName == "") {
        delete[] waveformDataL;
        delete[] waveformDataR;
        waveformDataL = nullptr;
        waveformDataR = nullptr;
    }

	alGenSources((ALuint)1, &source);
	testALError("source generation");

	alSourcef(source, AL_PITCH, 1);
	testALError("source pitch");
	alSourcef(source, AL_GAIN, 1);
	testALError("source gain");
	alSource3f(source, AL_POSITION, 0, 0, 0);
	testALError("source position");
	alSource3f(source, AL_VELOCITY, 0, 0, 0);
	testALError("source velocity");
	alSourcei(source, AL_LOOPING, AL_FALSE);
	testALError("source looping");

	alGenBuffers(1, &buffer);
	testALError("buffer generation");

    data = load_wav(std::string(fileName), channels, sampleRate, bitsPerSample, dataSize);
    if(!data)
    {
        std::cerr << "ERROR: Could not load wav" << std::endl;
        return;
    }

	buffer = alutCreateBufferFromFile(fileName);

    alGenBuffers(NUM_BUFFERS, &buffers[0]);

    if(channels == 1 && bitsPerSample == 8)
        format = AL_FORMAT_MONO8;
    else if(channels == 1 && bitsPerSample == 16)
        format = AL_FORMAT_MONO16;
    else if(channels == 2 && bitsPerSample == 8)
        format = AL_FORMAT_STEREO8;
    else if(channels == 2 && bitsPerSample == 16)
        format = AL_FORMAT_STEREO16;
    else
    {
        std::cerr
            << "ERROR: unrecognised wave format: "
            << channels << " channels, "
            << bitsPerSample << " bps" << std::endl;
        return;
    }

    int frameByteCount = getFrameSizeAsBytes(_fps);
    for(std::size_t i = 0; i < NUM_BUFFERS; ++i)
    {
        alBufferData(buffers[i], format, &data[i * frameByteCount], frameByteCount, sampleRate);
    }

    alSourceQueueBuffers(source, NUM_BUFFERS, &buffers[0]);

    // std::cout << "Params from load_wav: " << std::endl;
    // std::cout << "numchannels: " << (int)channels << std::endl;
    // std::cout << "sampleRate: " << (int)sampleRate << std::endl;
    // std::cout << "bitsPerSample: " << (int)bitsPerSample << std::endl;
    // std::cout << "dataSize: " << (int)dataSize << std::endl;
    // std::cout << "format: " << format << std::endl;

    // ALint size, bits, channels, freq;
	// alGetBufferi(buffer, AL_SIZE, &size);
	// alGetBufferi(buffer, AL_BITS, &bits);
	// alGetBufferi(buffer, AL_CHANNELS, &channels);
	// alGetBufferi(buffer, AL_FREQUENCY, &freq);

    // std::cout << "Params from buffer: " << std::endl;
    // std::cout << "size: " << (int)size << std::endl;
    // std::cout << "bits: " << (int)bits << std::endl;
    // std::cout << "channels: " << (int)channels << std::endl;
    // std::cout << "freq: " << (int)freq << std::endl;

    _fileLoaded = true;
}


void AudioHandler::generateWaveform(int pixelWidth, int pixelHeight)
{
    delete[] waveformDataL;
    delete[] waveformDataR;
    waveformDataL = nullptr;
    waveformDataR = nullptr;

    if (!fileLoaded())
        return;

    waveformDataL = new float[pixelWidth];
    waveformDataR = new float[pixelWidth];

    float height = pixelHeight;
    char* rawData = getData();
    int bitsPerSample = getBitsPerSample();
    float maxSampleValue = pow(2.0, bitsPerSample);

    // Number of samples to average over per pixel is a product of:
    // Buffer length divided with pixel count gives length in seconds per pixel
    // This multiplied with sample rate gives number of samples per pixel
    int averageOver = (getBufferLengthInSec(buffer) / (float)pixelWidth) * (float)sampleRate;

    // std::cout << "Pixel width: " << pixelWidth << std::endl;
    // std::cout << "Length in seconds: " << getBufferLengthInSec(buffer) << std::endl;
    // std::cout << "Sample rate: " << sampleRate << std::endl;
    // std::cout << "Averageover: " << averageOver << std::endl;

    // Iterate over width of graph pixels this is how wide the graph will be.
    for (int x = 0; x < pixelWidth; ++x) {
        float negSumL = 0.0;
        float posSumL = 0.0;
        float negSumR = 0.0;
        float posSumR = 0.0;
        int samplePosL;
        int samplePosR;

        for (int i = 0; i < averageOver; ++i) {
            // Calculate normalized sample value
            // Position is a product of pixel, number of samples per pixel, number of channels and bit depth
            // int sampleOffset = averageOver * channels * (bitsPerSample / 8);
            samplePosL = (x * averageOver + i) * channels * (bitsPerSample / 8);
            samplePosR = (x * averageOver + i) * channels * (bitsPerSample / 8) + (bitsPerSample / 8);
            if (samplePosR < dataSize) {
                float sampleValueL = convert_to_short(&rawData[samplePosL], bitsPerSample / 8, false) / maxSampleValue;
                float sampleValueR = convert_to_short(&rawData[samplePosR], bitsPerSample / 8, false) / maxSampleValue;

                if (sampleValueL > 0)
                    posSumL += sampleValueL;
                else
                    negSumL += sampleValueL;

                if (sampleValueR > 0)
                    posSumR += sampleValueR;
                else
                    negSumR += sampleValueR;
            }
        }
        float finalValueL = (posSumL + std::abs(negSumL)) / (float)averageOver;
        float finalValueR = (posSumR + std::abs(negSumR)) / (float)averageOver;
        waveformDataL[x] = finalValueL;
        waveformDataR[x] = finalValueR;

        // Probe stuff from last col
        // if (x + 1 == pixelWidth) {
        //     std::cout << "Column: " << x << std::endl;
        //     std::cout << "samplePosL: " << samplePosL << std::endl;
        //     std::cout << "samplePosR: " << samplePosR << std::endl;
        // }
    }

    waveformWidth = pixelWidth;
}


void AudioHandler::playFile()
{
    ALint source_state;
    alGetSourcei(source, AL_SOURCE_STATE, &source_state);
	testALError("source state get");

	if (source_state == AL_PLAYING)
        alSourceStop(source);

    alSourcePlay(source);
}


void AudioHandler::playFrame(int frame)
{
    ALint source_state;

    // Update last played frame only if updating the buffer queue succeeded
    // If it fails, next frame will try to add to queue again
    if (update_stream(source, format, sampleRate, data, dataSize, frame))
        _lastPlayedFrame = frame;
    
    alGetSourcei(source, AL_SOURCE_STATE, &source_state);
	testALError("source state get");

    if (source_state != AL_PLAYING)
        alSourcePlay(source);
}


void AudioHandler::setFps(float newFps)
{
    _fps = newFps;
}


void AudioHandler::setGain(float gain)
{
    alSourcef(source, AL_GAIN, gain);
    testALError("source gain");
}


void AudioHandler::setPitch(float pitch)
{
    alSourcef(source, AL_PITCH, pitch);
    testALError("source pitch");
}


void AudioHandler::setChunkSize(int chunkSize)
{
    _chunkSize = chunkSize;
}


int AudioHandler::getFrameSizeAsBytes(float fps)
{
    if (!fileLoaded())
        return  0;
        
    ALfloat fileLength = getBufferLengthInSec(buffer);

    ALint size, bits, channels, freq;
	alGetBufferi(buffer, AL_SIZE, &size);
	alGetBufferi(buffer, AL_BITS, &bits);
	alGetBufferi(buffer, AL_CHANNELS, &channels);
	alGetBufferi(buffer, AL_FREQUENCY, &freq);
	if (alGetError() != AL_NO_ERROR)
		return 0;

    // Divide byte count with frame count
    ALint numBytesPerFrame = size / (fileLength * fps);
    return numBytesPerFrame;
}

int AudioHandler::getFileLengthInFrames()
{
    if (!fileLoaded())
        return 0;

    return (int)(getBufferLengthInSec(buffer) * _fps);
}


int AudioHandler::getEmptyBuffersCount()
{
    ALint buffersProcessed = 0;
    alGetSourcei(source, AL_BUFFERS_PROCESSED, &buffersProcessed);
    if (buffersProcessed <= 0)
        return 0;
    
    return buffersProcessed;
}


std::vector<int> AudioHandler::getFramesInQueue()
{
    return queueFrames;
}


bool AudioHandler::update_stream(const ALuint source,
                   const ALenum& format,
                   const std::int32_t& sampleRate,
                   char* soundData,
                   int soundDataSize,
                   int currentFrame)
{
    ALint buffersProcessed = 0;
    alGetSourcei(source, AL_BUFFERS_PROCESSED, &buffersProcessed);

    if (buffersProcessed <= 0)
        return false;

    int chunkByteCount = getFrameSizeAsBytes(_fps) * _chunkSize;
    // int frameChunk = currentFrame / _chunkSize;
    int frameChunk = (currentFrame - (currentFrame % _chunkSize)) / _chunkSize;

    // Remove all used buffers from queue
    ALuint frameBuffer;
    while(buffersProcessed--)
    {
        alSourceUnqueueBuffers(source, 1, &frameBuffer);
    }

    char* bufferData = new char[chunkByteCount];
    std::memset(bufferData, 0, chunkByteCount);

    std::size_t dataSizeToCopy = chunkByteCount;
    if (frameChunk * chunkByteCount + chunkByteCount > soundDataSize)
        dataSizeToCopy = soundDataSize - chunkByteCount;

    std::memcpy(&bufferData[0], &soundData[frameChunk * chunkByteCount], dataSizeToCopy);

    alBufferData(frameBuffer, format, bufferData, chunkByteCount, sampleRate);
    alSourceQueueBuffers(source, 1, &frameBuffer);

    delete[] bufferData;

    return true;
}