
#include "audioHandler.h"

#include "DDImage/Iop.h"
#include "DDImage/NukeWrapper.h"
#include "DDImage/Row.h"
#include "DDImage/Tile.h"
#include "DDImage/Knobs.h"
#include "DDImage/Thread.h"

// General includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <math.h>
#include <bitset>
#include <assert.h>
#include <cmath>
#include <stdint.h>
#include <thread>
#include <chrono>
#include <errno.h>
#include <inttypes.h>
#include <unistd.h>
#include <stdbool.h>
#include <cstring>
#include <fstream>


// OpenAL
#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alut.h>

AudioHandler audioHandler;

typedef std::chrono::high_resolution_clock Clock;

static const char* const CLASS = "AudioPlayer";
static const char* const HELP = "Plays audio in Nuke nodegraph interface\n Version 1.0";

#define MAX_INPUTS 1

using namespace DD::Image;


class AudioPlayer : public Iop
{
    // Knob specific variables
	const char* _fileKnob;
	bool _loop;
    bool _showWaveform;
	int _offset;
    float _gain;
	float _pitch;
	float _fps;
	float _waveformHeight;
	int _chunkSize;
    bool _scrub;
	
    DD::Image::Knob* knobShowWaveform;
	DD::Image::Knob* knobLoop;
	DD::Image::Knob* knobOffset;
    DD::Image::Knob* knobGain;
	DD::Image::Knob* knobPitch;
	DD::Image::Knob* knobFPS;
	DD::Image::Knob* knobWaveformHeight;
	DD::Image::Knob* knobChunkSize;
    DD::Image::Knob* knobScrub;

    // Operational variables
    Lock _lock;
    bool _firstTime;
	bool _loggingEnabled;
	int randomizer;
    int lastRequestY;
    int lastRequestT;
    int threadsProcessed;

public:

    int maximum_inputs() const { return MAX_INPUTS; }
    int minimum_inputs() const { return MAX_INPUTS; }

    AudioPlayer (Node* node) : Iop (node) {
		_fileKnob = "";
		_offset = 0;
		_loop = false;
        _showWaveform = true;
        _gain = 1.0f;
		_pitch = 1.0f;
		_fps = 25.0f;
		_waveformHeight = 50.0f;
		_chunkSize = 12;
        _scrub = false;
        _firstTime = true;
		_loggingEnabled = true;
		randomizer = 0;
        lastRequestY = 0;
        lastRequestT = 0;
        threadsProcessed = 0;
    }

    ~AudioPlayer () {
    }

    void logMessage(std::string message, bool useDivider = false);
    const char* input_label(int input, char* buffer) const;
    void knobs(Knob_Callback f) override;
    int knob_changed(DD::Image::Knob* k);
    void _validate(bool);
    void _request(int x, int y, int r, int t, ChannelMask channels, int count);
    void _open();
	void append(DD::Image::Hash& hash) override;

    void engine ( int y, int x, int r, ChannelMask channels, Row& out );

    static const Description desc;
    const char* Class() const override { return CLASS; }
    const char* node_help() const override { return HELP; }
    const char* displayName() const { return "AudioPlayer"; }
};


static Iop* build(Node* node) { return new AudioPlayer(node); }
const Iop::Description AudioPlayer::desc(CLASS, "Other/AudioPlayer", build);



// ---------------------------------------------------------------------------
// Debug helper method for generic logging
// ---------------------------------------------------------------------------
void AudioPlayer::logMessage(std::string message, bool useDivider)
{
    if (_loggingEnabled) {
        std::cout << message << std::endl;
        if (useDivider)
            std::cout << "-----------------------------------------------" << std::endl;
    }
}


const char* AudioPlayer::input_label(int input, char* buffer) const
{
    switch (input)
    {
        case 0:
            return "input";
        case 1:
            return "audio";
        default:
            return "input";
    }
}


// ---------------------------------------------------------------------------
// Knob setup
// ---------------------------------------------------------------------------
void AudioPlayer::knobs(Knob_Callback f)
{
	File_knob(f, &_fileKnob, "file_name", "WAV file");
	DD::Image::Tooltip(f, "Select .wav audio file, must be in standard PCM format.");

    knobShowWaveform = Bool_knob(f, &_showWaveform, "show_waveform", "Show waveform");
	SetFlags(f, DD::Image::Knob::STARTLINE);
	DD::Image::Tooltip(f, "Renders waveform on top of input image, scaled to width. Red is left channel, green is right channel, current time marker in blue channel.");

	knobLoop = Bool_knob(f, &_loop, "loop", "Loop");
	SetFlags(f, DD::Image::Knob::STARTLINE);
	DD::Image::Tooltip(f, "Loop audio. Wraps around at clip end.");

	knobOffset = Int_knob(f, &_offset, "offset", "Offset");
	SetFlags(f, DD::Image::Knob::STARTLINE);
	DD::Image::Tooltip(f, "Change audio start offset in frames. Positive values shift start forward, negative backward in time.");

    knobFPS = Float_knob(f, &_fps, "fps", "FPS");
    SetFlags(f, DD::Image::Knob::STARTLINE);
    DD::Image::Tooltip(f, "Change audio fps. NB! This should match project fps because it dictates the relationship between Nuke playback speed and audio playback timing.");

    knobGain = Float_knob(f, &_gain, "gain", "Gain");
    SetFlags(f, DD::Image::Knob::STARTLINE);
    DD::Image::Tooltip(f, "Change audio gain.");

    knobPitch = Float_knob(f, &_pitch, "pitch", "Pitch");
    SetFlags(f, DD::Image::Knob::STARTLINE);
    DD::Image::Tooltip(f, "Change audio pitch.");

    knobWaveformHeight = Float_knob(f, &_waveformHeight, "waveform_height", "Waveform height");
    SetFlags(f, DD::Image::Knob::STARTLINE);
    SetRange(f, 0.0, 200.0);
    DD::Image::Tooltip(f, "Change waveform draw height.");

    Divider(f, "Technicals");

	knobChunkSize = Int_knob(f, &_chunkSize, "chunk_size", "Chunk Size");
	SetFlags(f, DD::Image::Knob::STARTLINE);
	DD::Image::Tooltip(f, "Size of audio chunks (in frame length) that are sent to queue. This controls how long is the minimal piece of audio played. Setting it longer than single frame helps remove crackling which occurs due to buffer underrun.\n\nValues above 12 are suggested for cleaner playback.");

    knobScrub = Bool_knob(f, &_scrub, "scrub", "Enable better scrubbing. NB! Better scrubbing WILL break renders! Use ONLY for audio investigation purposes!");
	SetFlags(f, DD::Image::Knob::STARTLINE);
	DD::Image::Tooltip(f, "Does a nasty trick to allow better audio scrubbing. Breaks continuous playback and renders!.");

    Divider(f, "");

	std::string infoString = "AudioPlayer v1.0 by Hendrik Proosa\nBuild ";
    infoString += __TIMESTAMP__;
    DD::Image::Text_knob(f, "", infoString.c_str());
    DD::Image::SetFlags(f, DD::Image::Knob::DISABLED);


    Iop::knobs(f);
}


// ---------------------------------------------------------------------------
// Op::knob_changed gets called when UI knobs are meddled with
// ---------------------------------------------------------------------------
int AudioPlayer::knob_changed(DD::Image::Knob* k)
{
	if (k->is("file_name")) {
		audioHandler.setFileLoaded(false);
        return 1;
    }

	if (k->is("gain")) {
        audioHandler.setGain(_gain);
        return 1;
    }

	if (k->is("pitch")) {
        audioHandler.setPitch(_pitch);
        return 1;
    }

	if (k->is("chunk_size")) {
        audioHandler.setChunkSize(_chunkSize);
        return 1;
    }

	if (k->is("fps")) {
        audioHandler.setFileLoaded(false);
        audioHandler.setFps(_fps);
        return 1;
    }

    return Iop::knob_changed(k);
}

void AudioPlayer::append(DD::Image::Hash& hash)
{
	hash.append(outputContext().frame());
	hash.append(randomizer);
}


// ---------------------------------------------------------------------------
// _validate has to provide bounding box and other relevant stuff for Nuke
// ---------------------------------------------------------------------------
void AudioPlayer::_validate(bool for_real)
{
    // Validate all inputs
    for (int i = 0; i < MAX_INPUTS; ++i)
    {
        if (input(i))
            input(i)->validate(for_real);
    }

    copy_info();
	invalidateSameHash();
}


// ---------------------------------------------------------------------------
// _request signals plugin, which area Nuke engine wants us to render
// ---------------------------------------------------------------------------
void AudioPlayer::_request(int x, int y, int r, int t, ChannelMask channels, int count)
{
    // request all input input as we are going to search the whole input area
    ChannelSet readChannels = input0().info().channels();
    input(0)->request( readChannels, count );

    lastRequestY = y;
    lastRequestT = t;
    threadsProcessed = 0;
}


// ---------------------------------------------------------------------------
// _open method is called by Nuke engine when new instance of op is created
// ---------------------------------------------------------------------------
void AudioPlayer::_open()
{
    if (_fileKnob == "")
        audioHandler.setFileLoaded(false);

    // Reload file and regenerate waveform if file isn't loaded or drawing width of waveform has changed
	if (!audioHandler.fileLoaded() || audioHandler.getWaveformWidth() != input0().format().width()) {
        if (_fileKnob != "") {
            audioHandler.setFps(_fps);
            audioHandler.loadFile(_fileKnob);
            audioHandler.generateWaveform(input0().format().width(), 100);
        }
	}

    _firstTime = true;
    threadsProcessed = 0;
}



// ---------------------------------------------------------------------------
// Main processing
// ---------------------------------------------------------------------------
// First pass does all the patching
// Second pass is lookup to find if patch id-s are within masked ids
void AudioPlayer::engine ( int y, int x, int r, ChannelMask channels, Row& row )
{
    // --------------------------------------------------------
    // FIRST PASS
    // --------------------------------------------------------
    // Run on single thread by engine, so any multithreading
    // must be handled by plugin itself.

    if (audioHandler.fileLoaded()) {
        Guard guard(_lock);
        threadsProcessed++;
        // std::cout << "thr: " << threadsProcessed << " Req:" << (lastRequestT - lastRequestY) << std::endl;
        // Enable nasty scrubbing trick
        // if (_scrub && threadsProcessed == (lastRequestT - lastRequestY))
        //     abort();

        if (_firstTime) {
            int chunkSize = _chunkSize;

            // Set chunk to single frame for scrub trick, otherwise sound will be strange
            if (_scrub)
                chunkSize = 1;

            audioHandler.setChunkSize(chunkSize);
            audioHandler.setGain(_gain);

            // Play only when offset after clip start, offset buffer by amount of filled buffers
            int requestFrame = outputContext().frame() - _offset;
            bool getNewFrame = true;

            if (_loop)
                requestFrame = requestFrame % audioHandler.getFileLengthInFrames();

            // Don't try to get new frame if out of range or already played this frame
            if (requestFrame < 0)
                getNewFrame = false;

            if (requestFrame >= (audioHandler.getFileLengthInFrames() - chunkSize))
                getNewFrame = false;

            if (audioHandler.getLastPlayedFrame() == requestFrame)
                getNewFrame = false;

            // For example if we do 25 frame chunks, frame 30 has offset of 5 from start and
            // chunk start for it will be frame 25
            int offsetFromChunkStart = requestFrame % chunkSize;
            int chunkStartFrame = requestFrame - offsetFromChunkStart;
            int offsetFromNextChunk = chunkSize - offsetFromChunkStart;
            int nextChunkStartFrame = chunkStartFrame + chunkSize;

            // Haven't played this chunk yet
            if (getNewFrame && offsetFromChunkStart >= 0 && audioHandler.getLastPlayedFrame() != chunkStartFrame)
                audioHandler.playFrame(chunkStartFrame);

            randomizer++;
            _firstTime = false;
        }
    }

    // --------------------------------------------------------
    // SECOND PASS
    // --------------------------------------------------------
    // Writes data we produced in first pass to pixels in multithreaded
    // scanline based write
    Row in( x, r );
    in.get( input0(), y, x, r, channels );
    if ( aborted() )
        return;

    // Enable nasty scrubbing trick
    if (_scrub && y == lastRequestT - 1)
        abort();

    if (audioHandler.fileLoaded() && _showWaveform) {
        int maxWidth = input0().format().width();
        float* waveformL = audioHandler.getWaveformL();
        float* waveformR = audioHandler.getWaveformR();
        int frameWrapBack = ((int)outputContext().frame() - _offset) % audioHandler.getFileLengthInFrames();
        float cursorRelative = (float)frameWrapBack / audioHandler.getFileLengthInFrames();
        int cursorPos = cursorRelative * maxWidth;

        foreach( z, channels ) {
            float *CUR = row.writable(z) + x;
            const float* inptr = in[z] + x;
            const float *END = row[z] + r;
            int pos = x;
            float outValue = 0.0;
            while ( CUR < END ) {
                float audioLevel = 0.0;
                int maxLevel = 0;
                int avgLevel = 0;
                outValue = *inptr;

                if (z == DD::Image::Chan_Red) {
                    if (pos < maxWidth && waveformL && y < (waveformL[pos] * _waveformHeight * 100.0))
                        outValue = 1.0; //waveformL[pos];
                }

                if (z == DD::Image::Chan_Green) {
                    if (pos < maxWidth && waveformR && y < (waveformR[pos] * _waveformHeight * 100.0))
                        outValue = 1.0; //waveformR[pos];
                }

                if (z == DD::Image::Chan_Blue && pos == cursorPos)
                    outValue = 1.0;

                *CUR = outValue;
                CUR++;
                inptr++;
                pos++;
            }
        }
    } else {
        foreach( z, channels ) {
            float *CUR = row.writable(z) + x;
            const float* inptr = in[z] + x;
            const float *END = row[z] + r;
            while ( CUR < END ) {
                float outValue = *inptr;
                *CUR = outValue;
                CUR++;
                inptr++;
            }
        }
    }
}