#HP tools
nuke.pluginAddPath('./hpTools/icons')
nMajor = nuke.NUKE_VERSION_MAJOR
nMinor = nuke.NUKE_VERSION_MINOR
if nMajor == 11 and nMinor == 3:
    nuke.pluginAddPath('./hpTools/Nuke11.3')
if nMajor == 12 and nMinor == 0:
    nuke.pluginAddPath('./hpTools/Nuke12.0')
if nMajor == 12 and nMinor == 1:
    nuke.pluginAddPath('./hpTools/Nuke12.1')
if nMajor == 12 and nMinor == 2:
    nuke.pluginAddPath('./hpTools/Nuke12.2')
